package com.example.mafer.intropart2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mafer on 22/02/2018.
 */

public class Hostel implements Parcelable {
    private Mascota cat;
    private Mascota dog;
    private Mascota dog1;
    private Mascota cat1;
    private Mascota parrot;

    public Hostel(Mascota cat, Mascota dog, Mascota dog1, Mascota cat1, Mascota parrot){
        this.cat = cat;
        this.dog = dog;
        this.cat1 = cat1;
        this.dog1 = dog1;
        this.parrot = parrot;
    }

    public Mascota getDog() {
        return dog;
    }

    public Mascota getDog1() {
        return dog1;
    }

    public Mascota getCat1() {
        return cat1;
    }

    public Mascota getParrot() {
        return parrot;
    }

    public Mascota getCat(){
        return cat;

    }


    protected Hostel(Parcel in) {
        cat = (Mascota) in.readValue(Mascota.class.getClassLoader());
        dog = (Mascota) in.readValue(Mascota.class.getClassLoader());
        dog1 = (Mascota) in.readValue(Mascota.class.getClassLoader());
        cat1 = (Mascota) in.readValue(Mascota.class.getClassLoader());
        parrot = (Mascota) in.readValue(Mascota.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(cat);
        dest.writeValue(dog);
        dest.writeValue(dog1);
        dest.writeValue(cat1);
        dest.writeValue(parrot);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Hostel> CREATOR = new Parcelable.Creator<Hostel>() {
        @Override
        public Hostel createFromParcel(Parcel in) {
            return new Hostel(in);
        }

        @Override
        public Hostel[] newArray(int size) {
            return new Hostel[size];
        }
    };
}