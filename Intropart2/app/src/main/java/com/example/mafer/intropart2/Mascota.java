package com.example.mafer.intropart2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mafer on 22/02/2018.
 */

public class Mascota implements Parcelable {
    private String name;
    private String animal;
    private String age;
    private String color;

    public Mascota(String name, String animal, String age, String color){
        this.name = name;
        this.age = age;
        this.animal = animal;
        this.color = color;

    }

    public String getName(){
        return name;
    }

    public String getAnimal(){
        return animal;
    }

    public String getColor() {
        return color;
    }

    public String getAge(){
        return age;
    }

    protected Mascota(Parcel in) {
        name = in.readString();
        animal = in.readString();
        age = in.readString();
        color = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(animal);
        dest.writeString(age);
        dest.writeString(color);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Mascota> CREATOR = new Parcelable.Creator<Mascota>() {
        @Override
        public Mascota createFromParcel(Parcel in) {
            return new Mascota(in);
        }

        @Override
        public Mascota[] newArray(int size) {
            return new Mascota[size];
        }
    };
}