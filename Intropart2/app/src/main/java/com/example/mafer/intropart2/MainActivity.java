package com.example.mafer.intropart2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    ListView listaplanets;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listaplanets = (ListView)findViewById(R.id.lv01);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.listado, android.R.layout.simple_list_item_1);
        listaplanets.setAdapter(adapter);

        Mascota gato = new Mascota("luna", "gato", "1" , "blanca");
        Mascota dog1 = new Mascota("Zeus", "perro", "2", "blanco");
        Mascota dog = new Mascota ("Pichi", "perro", "1", "cafe");
        Mascota cat1 = new Mascota("Sasu", "gato", "3", "gris");
        Mascota parrot = new Mascota("Loro", "Loro", "1", "verde");

        listaplanets.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        //Abre la activity 2
                        Intent test = new Intent(view.getContext(), Main2Activity.class);
                        int item = i;
                        String itemVal = (String)listaplanets.getItemAtPosition(i);
                        Mascota gato = new Mascota("luna", "gato", "1" , "blanca");
                        Mascota dog1 = new Mascota("Zeus", "perro", "2", "blanco");
                        Mascota dog = new Mascota ("Pichi", "perro", "1", "cafe");
                        Mascota cat1 = new Mascota("Sasu", "gato", "3", "gris");
                        Mascota parrot = new Mascota("Loro", "Loro", "1", "verde");
                        Hostel mHostel = new Hostel(gato, dog, dog1, cat1, parrot);

                        test.putExtra("Hostel", mHostel);
                        startActivity(test);

                    }
                }
        );

    }
}
