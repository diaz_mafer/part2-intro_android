package com.example.mafer.intropart2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle recibir = getIntent().getExtras();
        Hostel text = recibir.getParcelable("Hostel");
        setContentView(R.layout.activity_main2);
        TextView planeta = findViewById(R.id.textView);
        TextView gato = findViewById(R.id.gato);
        TextView dog = findViewById(R.id.dog);
        TextView dog1 = findViewById(R.id.dog1);
        TextView par = findViewById(R.id.par);

        gato.setText(text.getCat1().getName() + " " + "Edad: " + text.getCat1().getAge() + " " + "Color: " + text.getCat1().getColor() + " " + "Tipo: " + text.getCat1().getAnimal());
        dog.setText(text.getDog().getName()+ " " + "Edad: " + text.getDog().getAge()  + " " + "Color: " + text.getDog().getColor() + " " + "Tipo: " + text.getDog().getAnimal());
        dog1.setText(text.getDog1().getName()+ " " + "Edad: " + text.getDog1().getAge() + " " + "Color: " + text.getDog1().getColor() + " " + "Tipo: " + text.getDog1().getAnimal());
        par.setText(text.getParrot().getName()+ " " + "Edad: " + text.getParrot().getAge() + " " + "Color: " + text.getParrot().getColor() + " " + "Tipo: " + text.getParrot().getAnimal());
        planeta.setText(text.getCat().getName()+ " " + "Edad: " + text.getCat().getAge() + " " + "Color: " + text.getCat().getColor() + " " + "Tipo: " + text.getCat().getAnimal());
    }
}
